import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  ScrollView
} from 'react-native';

import CalcButtons from './src/components/Buttons/index';
import CalcHistory from './src/components/History/index';

const buttons = [
  [{ text: 'C', val: 'C' }, { text: '-/+', val: '-/+' }, { text: 'Ro', val: 'Ro' }, { text: '←', val: '←' }],
  [{ text: '7', val: '7' }, { text: '8', val: '8' }, { text: '9', val: '9' }, { text: '÷', val: '÷' }],
  [{ text: '4', val: '4' }, { text: '5', val: '5' }, { text: '6', val: '6' }, { text: '*', val: '*' }],
  [{ text: '1', val: '1' }, { text: '2', val: '2' }, { text: '3', val: '3' }, { text: '-', val: '-' }],
  [{ text: '.', val: '.' }, { text: '0', val: '0' }, { text: '=', val: '=' }, { text: '+', val: '+' }],
]

const romButtons = [
  [{ text: 'C', val: 'C' }, { text: '-/+', val: '-/+' }, { text: 'Ro', val: 'Ro' }, { text: '←', val: '←' }],
  [{ text: 'VII', val: '7' }, { text: 'VII', val: '8' }, { text: 'IX', val: '9' }, { text: '÷', val: '÷' }],
  [{ text: 'IV', val: '4' }, { text: 'V', val: '5' }, { text: 'XI', val: '6' }, { text: '*', val: '*' }],
  [{ text: 'I', val: '1' }, { text: 'II', val: '2' }, { text: 'III', val: '3' }, { text: '-', val: '-' }],
  [{ text: '.', val: '.' }, { text: '0', val: '0' }, { text: '=', val: '=' }, { text: '+', val: '+' }],
]

const romanize = async (num) => {
  var roman = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1,
    S: 0.5,
    '.': 0.83,
    '..': 0.17,
    '...': 0.25,
    '....': 0.33,
    '.....': 0.42,
    'S.': 0.58,
    'S..': 0.67,
    'S...': 0.75,
    'S....': 0.83,
    'S.....': 0.92
  };
  var str = '';

  for (var i of Object.keys(roman)) {
    var q = await Math.floor(num / roman[i]);
    num -= await q * roman[i];
    str += await i.repeat(q);
  }
  return str;
}

const expressionParser = async (data) => {
  let finalData = '', lastOp = '', parsedData = data

  for (let i = 0; i < data.length; i++) {
    const element = data.charAt(i);
    if (element === '+') {
      finalData = finalData + await romanize(parsedData.split('+')[0]) + '+'
      parsedData = data.split('+')[1]
      lastOp = '+'
    }
    if (element === '-') {
      finalData = finalData + await romanize(parsedData.split('-')[0]) + '-'
      parsedData = data.split('-')[1]
      lastOp = '-'
    }
    if (element === '/') {
      finalData = finalData + await romanize(parsedData.split('/')[0]) + '÷'
      parsedData = data.split('/')[1]
      lastOp = '/'
    }
    if (element === '*') {
      finalData = finalData + await romanize(parsedData.split('*')[0]) + '*'
      parsedData = data.split('*')[1]
      lastOp = '*'
    }
  }

  finalData = finalData + await romanize(parsedData)
  return finalData
}

const romanParser = async (data) => {
  let finalData = '', lastOp = '', parsedData = data

  for (let i = 0; i < data.length; i++) {
    const element = data.charAt(i);
    if (element === '+') {
      finalData = finalData + parsedData.split('+')[0] + '+'
      parsedData = data.split('+')[1]
      lastOp = '+'
    }
    if (element === '-') {
      finalData = finalData + parsedData.split('-')[0] + '-'
      parsedData = data.split('-')[1]
      lastOp = '-'
    }
    if (element === '/') {
      finalData = finalData + parsedData.split('/')[0] + '÷'
      parsedData = data.split('/')[1]
      lastOp = '/'
    }
    if (element === '*') {
      finalData = finalData + parsedData.split('*')[0] + '*'
      parsedData = data.split('*')[1]
      lastOp = '*'
    }
  }

  finalData = finalData + parsedData
  return finalData
}

const App = () => {
  const [output, setOutput] = useState(0);
  const [history, setHistory] = useState([]);
  const [expression, setExpression] = useState('0');
  const [totalIden, setTotalIden] = useState(false);
  const [roman, setRoman] = useState(false)

  const btnPress = async (data) => {
    if (output.length === 27 && data.val !== '=' && data.val !== 'C' && data.val !== 'Ro' && data.val !== '←' && data.val !== '-/+') {
      ToastAndroid.show("Maximum Expression Length of 27 is reached", 3)
    } else {
      switch (data.val) {
        case '=':
          if (data.val !== '+' && data.val !== '-' && data.val !== '/' && data.val !== '*') {
            let total = await eval(expression)
            if (roman) {
              var romNum = await romanize(total)
              setHistory(oldHist => [...oldHist, { output, total: romNum }])
              setOutput(romNum)
            } else {
              setHistory(oldHist => [...oldHist, { output, total }])
              setOutput(total)
            }
            setExpression(total.toString())
            setTotalIden(true)
          }

          break;

        case 'C':
          setExpression('0')
          setOutput('0')
          break;

        case 'Ro':
          if (roman) {
            setRoman(false)
            setOutput(await romanParser(expression))
            //setOutput(expression)
          } else {
            setRoman(true)
            //setOutput(romanize(expression))
            setOutput(await expressionParser(expression))
          }

          break;

        case '←':
          if (expression.length === 1 || expression === 'Infinity') {
            setExpression('0')
            setOutput(0)
          } else {
            if (roman) {
              setOutput(await expressionParser(expression.slice(0, -1)))
            } else {
              setOutput(output.toString().slice(0, -1))
            }
            setExpression(expression.slice(0, -1))

          }
          setTotalIden(false)
          break;

        case '-/+':
          if (expression !== '0' && expression.charAt(0) !== '-') {
            setExpression('-' + expression)
            setOutput('-' + output)
          } else if (expression.charAt(0) === '-') {
            setExpression(expression.substring(1))
            setOutput(output.substring(1))
          }
          setTotalIden(false)
          break;

        case '+':
          if (
            expression.charAt(expression.length - 1) !== '+' &&
            expression.charAt(expression.length - 1) !== '-' &&
            expression.charAt(expression.length - 1) !== '*' &&
            expression.charAt(expression.length - 1) !== '/' &&
            expression !== 'Infinity'
          ) {
            setExpression(expression + data.val)
            setOutput(output + data.text)
          }
          setTotalIden(false)

          break;

        case '-':
          if (
            expression.charAt(expression.length - 1) !== '+' &&
            expression.charAt(expression.length - 1) !== '-' &&
            expression.charAt(expression.length - 1) !== '*' &&
            expression.charAt(expression.length - 1) !== '/' &&
            expression !== 'Infinity'
          ) {
            setExpression(expression + data.val)
            setOutput(output + data.text)
          }
          setTotalIden(false)

          break;

        case '*':
          if (
            expression.charAt(expression.length - 1) !== '+' &&
            expression.charAt(expression.length - 1) !== '-' &&
            expression.charAt(expression.length - 1) !== '*' &&
            expression.charAt(expression.length - 1) !== '/' &&
            expression !== 'Infinity'
          ) {
            setExpression(expression + '*')
            setOutput(output + data.text)
          }
          setTotalIden(false)

          break;

        case '÷':
          if (
            expression.charAt(expression.length - 1) !== '+' &&
            expression.charAt(expression.length - 1) !== '-' &&
            expression.charAt(expression.length - 1) !== '*' &&
            expression.charAt(expression.length - 1) !== '/' &&
            expression !== 'Infinity'
          ) {
            setExpression(expression + '/')
            setOutput(output + data.text)
          }
          setTotalIden(false)

          break;

        case '.':
          if (
            expression.charAt(expression.length - 1) === '+' ||
            expression.charAt(expression.length - 1) === '-' ||
            expression.charAt(expression.length - 1) === '*' ||
            expression.charAt(expression.length - 1) === '/'
          ) {
            setExpression(expression + "0.")
            setOutput(output + "0.")
          } else if (expression.charAt(expression.length - 1) !== '.') {
            setExpression(expression + data.val)
            setOutput(output + data.text)
          }
          setTotalIden(false)

          break;

        default:
          if (expression === '0' || totalIden || expression === 'Infinity') {
            setExpression(data.val)
            setOutput(data.text)
            setTotalIden(false)
          } else {
            setExpression(expression + data.val)
            if (roman) {
              setOutput(await expressionParser(expression + data.val))
            } else {
              setOutput(output + data.text)
            }
          }
          break;
      }
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.clearContainer}>
        <Text
          onPress={() => setHistory([])}
          style={styles.clearText}>
          CLEAR HISTORY
        </Text>
      </View>
      <View style={styles.historyContainer}>
        <CalcHistory history={history} />
      </View>
      <View style={styles.inputContainer}>
        <ScrollView>
          <Text style={styles.outputText}>
            {output}
          </Text>
        </ScrollView>
      </View>
      <View style={styles.buttonContainer}>
        <CalcButtons
          buttons={roman ? romButtons : buttons}
          btnPress={btnPress}
          roman={roman}
        />
      </View>
      <View>

      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  buttonContainer: {
    flex: 0.5,
  },
  historyContainer: {
    flex: 0.4,
  },
  inputContainer: {
    flex: 0.1,
    justifyContent: 'center',
    padding: 4,
    borderWidth: 2,
    borderColor: '#C0C0C0'
  },
  outputText: {
    fontFamily: 'Helvetica-Light',
    fontSize: 30,
    textAlign: 'right',
    marginRight: 10
  },
  clearContainer: {
    height: 40,
    alignItems: 'flex-end',
    paddingRight: 15,
    justifyContent: 'center'
  },

  clearText: {
    color: '#2980b9',
    fontFamily: 'Helvetica-Light',
    fontSize: 15
  },

});

export default App;
