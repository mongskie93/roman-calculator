# README #

In the project directory, run the following:


### `npm install`

To install the packages needed to run the app.



## RUNNING USING ACTUAL DEVICE:

- connect the device via device and enable usb debugging.


### `adb devices`

To check if the device is properly connected to ADB (Android Debug Bridge)


### `adb reverse tcp:8081 tcp:8081`

To link the device to adb via USB.


### `npx react-native start`

To start Metro Bundler.


- Open new Terminal. Let the Metro Bundler run in its own Terminal.


### `npx react-native run-android`

To install/run the app to an android device.
