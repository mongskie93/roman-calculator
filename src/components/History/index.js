import React, { useRef } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView
} from 'react-native';


const History = (props) => {
    const { history } = props
    let x = 0
    const scrollViewRef = useRef();
    return (
        <ScrollView
            style={styles.container}
            ref={scrollViewRef}
            onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
        >
            {history && history.map((data) => {
                x++
                return (
                    <View key={x} style={{ borderBottomWidth: 1, borderBottomColor: 'black' }}>
                        <Text style={styles.txtDefault}>
                            {data.output}
                        </Text>
                        <Text style={styles.txtOutput}>
                            = {data.total}
                        </Text>
                    </View>
                )
            })}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    txtDefault: {
        color: '#000',
        fontFamily: 'Helvetica-Light',
        fontSize: 20,
        textAlign: 'right',
        padding: 10,
    },
    txtOutput: {
        color: '#dab600',
        fontFamily: 'Helvetica-Light',
        fontSize: 20,
        textAlign: 'right',
        padding: 10
    },

});

export default History;