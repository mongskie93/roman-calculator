import React from 'react';
import {
    TouchableNativeFeedback,
    StyleSheet,
    Text,
    View,
} from 'react-native';

const Button = (props) => {

    const { buttons, btnPress, roman } = props

    return (
        <View style={styles.container}>
            {
                buttons.map((row, index) => (
                    <View key={index} style={styles.contRow}>
                        {
                            row.map((col, index) => (
                                <TouchableNativeFeedback
                                    key={index}
                                    onPress={() => btnPress(col)}
                                //background={TouchableNativeFeedback.SelectableBackground()}
                                >
                                    <View style={(col.text === 'Ro' && roman) ? styles.contRoman : styles.contButton}>
                                        <Text style={styles.txtDefault}>{col.text}</Text>
                                    </View>
                                </TouchableNativeFeedback>
                            ))
                        }
                    </View>
                ))
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    txtDefault: {
        color: '#000',
        fontFamily: 'Helvetica-Light',
        fontSize: 20
    },
    contRow: {
        flex: 1,
        flexDirection: 'row'
    },

    contButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ecf0f1'
    },
    contZero: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ecf0f1'
    },

    contRoman: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        backgroundColor: '#dab600',
        borderColor: '#ecf0f1'
    }

});

export default Button;